package com.example.demo.user;

import org.neo4j.ogm.annotation.NodeEntity;

@NodeEntity
public class User {

	private Long id;

	private String name;

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
