package com.example.demo;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.user.User;
import com.example.demo.user.UserRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class DemoApplicationTests {

	@Autowired
	private UserRepository repository;

	@Test
	public void repositoryWorks() {
		User user = new User();
		user.setName("somebody");
		User savedUser = repository.save(user);

		User loadedUser = repository.findById(savedUser.getId()).get();
		assertThat(loadedUser.getName(), equalTo("somebody"));
	}

}
